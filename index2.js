require('require-yaml')
var Cylon = require('cylon')
var machina = require('machina')
var swig = require('swig')
var bone = require('bonescript')
var Emitter = require('events')
var LedBar = require('jsupm_my9221')
var cron = require('node-cron')

var iotar = require('./iotar2.yml') 

var arif = (item) => item ? (Array.isArray(item) ? item : [item] ) : null
var rend = (temp, data) => {
	var res =  (typeof temp === 'string') ? swig.render(temp, data ? {locals: data } : null) : temp
	if (res) console.log('Render:', temp, res)
	return res
}

var prep = (rule) => {
  if (!rule.in) rule.in = '*'
  if (!rule.on) rule.on = '_onEnter'
  if (!rule.do) rule.do = {}
  if (rule.at) rule.on = rule.at
  if (rule.do) for (var d in rule.do) rule.do[d] = arif(rule.do[d])
  if (rule.emit) rule.emit = arif(rule.emit)
  return rule
}

var load = (rule, iota) => {
  if (!iota.states[rule.in]) iota.states[rule.in] = {}
  var prior = iota.states[rule.in][rule.on]
  //if (rule.at) cron.schedule(rule.at, () => iota.handle(bot, r.at))

  var action = function(my) {
    if (prior) prior(my)
    console.log('rule:', rule)
    if (rend(rule.if, my.devs)) return 
    if (rule.emit) my.fsm.handle(my, rule.emit[0], rend(rule.emit[1], my.devs))
    if (rule.do) for (var d in rule.do) console.log(my.devs[d][rule.do[d][0]](rend(rule.do[d][1], my.devs)))
    if (rule.go) my.fsm.transition(my, rule.go)
  }

  if (rule.wait) { 
	  var temp = action
	  action = (my) => {
		  my.waits.forEach(clearTimeout)
		  my.waits = [after((rule.wait).seconds(), () => temp(my)) ]
	  }
  }

  if (rule.loop) { 
	  var temp = action
	  action = (my) => { 
		  my.loops.forEach(clearInterval)
		  my.loops = [every((rule.loop).seconds(), () => temp(my))]
	  }
  }
  iota.states[rule.in][rule.on] = action
}

var fsm = new machina.BehavioralFsm({
  initialState: iotar.start, 
  states: { 
    '*': { 
      _onExit: (my) => { 
	      my.waits.forEach(clearTimeout)
	      my.loops.forEach(clearInterval)
	      my.loops = [] 
	      my.waits = [] 
      } 
    } 
  }
})

fsm.on("*", (event, data) => delete data.client && console.log("EVENT:", event, data))

iotar.rules.forEach(rule => load(prep(rule), fsm))

var DigitalInput = function(pin='P9_12', on='on', off='off', maxStreak=1, interval=100, timeout=0) { 
	bone.pinMode(pin, bone.INPUT) 
	var sensor = new Emitter()
	var oldVal = null
	var streak = 0
	var detect  = function()  { 
  	var newVal = bone.digitalRead(pin)
		if (newVal == oldVal) return (++streak < maxStreak) ? sensor.emit((oldVal ? on : off )+'-'+streak) : null
		streak = 0
  	oldVal = newVal
		return sensor.emit(newVal ? on : off)
	}
	sensor.events = [on, off]
	var timer = setInterval(detect, interval)
	if (timeout) setTimeout(() => clearInterval(timer), timeout)   
	return sensor
}

var LedTestLight = function(dataPin=37, clockPin=38, instances=1) { 
	var ledBar = new LedBar.GroveLEDBar(dataPin, clockPin, instances)
	ledBar.setLevel = function(level) { 
		ledBar.level = level > 10 ? 10 : (level < 0 ? 0 : level)
		ledBar.setBarLevel(ledBar.level)
	}
	ledBar.incLevel = function(inc) { ledBar.setLevel(ledBar.level + inc) } 
	ledBar.setLevel(0)
	return ledBar
}

var bot  ={ 
	devs : { 
    ledBar : LedTestLight(37, 38), 
    motion : DigitalInput('P9_12', 'moves', 'still'),
		button : DigitalInput('P9_14', 'click')
	},
	fsm: fsm,
  waits: [],
	loops: [],
}

for (let r of iotar.rules) if (r.at) { 
	console.log('scheduling:', r.at) 
	cron.schedule(r.at, () => fsm.handle(bot, r.at))
}

for (let d in bot.devs) if (bot.devs[d].events) {
  for (let e of bot.devs[d].events) bot.devs[d].on(e, (name, data) => fsm.handle(bot, e, name, data))
}

console.log(bot.devs.motion) 
fsm.handle(bot, 'button')

every((30).seconds(), () => fsm.handle(bot, 'button'))
