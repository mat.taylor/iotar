var b = require('bonescript')
var Emitter = require('events')
var LedBar = require("jsupm_my9221")
var ledBar = new LedBar.GroveLEDBar(37, 38, 1)

/*
var moveRL = false 

//var ledInt = setInterval(() => lights(1, moveRL) , (1000 + 10*50))
var ledInt = setInterval(() => lights(1, moveRL) , (350 + 10*50))

var lights  = function(level, direction) {
    if (level >  10) moveRL = !moveRL
    else { 
	ledBar.setBarLevel(level, moveRL)
    	setTimeout(lights, 50, ++level, moveRL)    
    }
}

// When exiting: clear LED strip lights, clear interval, print exit message
process.on('SIGINT', () => {
   ledBar.setBarLevel(0, true)
   clearInterval(ledInt)
   console.log("Exiting...")
   process.exit(0)
})
*/

function DigitalInput(pin='P9_14', on='on', off='off', interval=100, timeout=0) { 
	b.pinMode(pin, b.INPUT)
	var sensor = new Emitter()
	var oldVal = null
	var detect = function() { 
		var newVal = b.digitalRead(pin)
  		if (newVal != oldVal) sensor.emit(newVal ? on : off)
  		oldVal = newVal
	}
	var timer = setInterval(detect, interval)
	if (timeout) setTimeout(() => clearInterval(timer), timeout)   
	return sensor
}

	

var myMotion = DigitalInput('P9_12', 'moving', 'noMove')
myMotion.on('moving', () => console.log('Motion Detected'))
myMotion.on('noMove', () => console.log('No Movement'))
var myButton = DigitalInput('P9_14')
myButton.on('on', () => console.log('Button On')) 
myButton.on('off', () => console.log('Button Off')) 

var brighter = () => (ledBar.level < 10) && ledBar.setBarLevel(++ledBar.level) 
var dimmer = () => (ledBar.level > 0) && ledBar.setBarLevel(--ledBar.level) 

var toggle = (func, interval=3) => { 
	if (timer) clearInterval(timer) 
	timer = setInterval(func, interval*100)
}

ledBar.level = 0
var timer = null //setInterval(dimmer, 300) 


//myMotion.on('moving', () => toggle(brighter, 1))
//myMotion.on('noMove', () => toggle(dimmer, 10))

console.log(myMotion)
