

Yes interesting chat today.

Im wondering if CSS is really the right starting point for the kind of thing you are after.

What do you think about just having a single URL addressable YAML/JSON doc which describes the current state of a ‘thing’ and the rules that govern the ‘thing’

eg..

```yaml
# Simple rule to switch lights on when motion is detected
id: /lights/1
state: 
   dimmer: 0.5
   switch: on
sense: 
	motion: 0.5 
rules: 
  r1: 
    when: 
      - { url: '#sense/motion', gt: 0.5 }
    then:
      - { url: '#state/dimmer', val: 0.9 }
```

Rules could then be chained by activated each 


Complex Rule state chaining

```yaml
id: '/lights/1'
rules: 
  r1: # start timer when no motion detected 
    when: 
      - { url: '#sense.motion', eq: 0 }
      - { url: '#state.switch', eq: on }
    then: 
      -	{ url: '#state.timer1', val: '$clock.timer()' }
  r2: # switch power of when no motion for 10 mins
    when: 
      - { url: '#state.switch', eq: 'on' }
      - { url: '#state.timer1', gt: 600  }
    then: 
      - { url: '#state.switch', val: 'off' }
      - { url: '#state.timer1', val: null  }
  r3: # switch lights back on when motion detected
    when: 
      - { url: '#sense.motion', gt: 0.5 }
      - { url: '#state.switch', eq: true }      
    then: 
      - { url: '#state.switch', val: 'on' } 
      - { url: '#state.timer1', val: null } 
        
```

			
      
Listening and responding to remote nodes

```yaml
id: /lights/1
# Use JSON Schema conventions for interiting rules from remote sources
allOf: [ { $ref: 'iota.io/rules/light' } ] 
rules:
  r1:
    # Listen for any motion detector on any light in the network
    when: 
    -  { url: '/lights/#items[*].sense.motion', gt: '0.5' }
    then: 
    -  { url: '#state.switch', val: 'on' }
```

      
Publishing and responsing to a topic

```yaml
id: /lights/2
rules:
  r1: # Set the state of the house to occupied when motion detected
    when: 
      - { url: '#sense.motion',   gt: 0.5   }
      - { url: '/house#occupied', eq: false }
    then: 
      - { url: '/house#occupied', val: true }
   r2: # when house is occupied turn the light on.
    when: 
      - { url: '/house#occupied', eq: true }
    then: 
      - { url: '#state.switch', val: 'on' }
```


```
state:
  default:
    colour: green
    mode: 
  emergency:


rules: 
  states: [ default, emergency]  
  allOf: 
      src: #sense
      url: abc.com/topic1, path: 'state.name' eq: 'emergency'
    anyOf: 

states:    
  default:
     
  state1:
    colour: white
    bright: 0.5
    switch: on

  state2: 
    period: 1
    moveTo: state2

rules:
  
  notify: [topic1, topic2, topic3]
       
model:
  color: 
state: 
  default: 
  initial: 
rules: 
  