require('require-yaml')
var Cylon = require("cylon")
var machina = require('machina')
var swig = require('swig')
var iotar = require('./demo1.yml')
//var Web3 = require('web3')
//var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"))

var toArray = (item) => Array.isArray(item) ? item : [item]

var handler = function (rules) {
  if (typeof rules === 'string') return rules
  rules = toArray(rules)
  for (var r in rules) rules[r] = lambada(rules[r]) 
  return function(my) {
    this.rules = rules
    console.log('HANDLING:', this)  
    rules.forEach((rule) => rule(my, this)) 
  }
}

var lambada = function(rule) {
  var action = function(my, fsm) {  
    if (rule.when) if (!swig.render(rule.when, { locals: my})) return
    if (rule.exec) for (device in rule.exec) {
      rule.exec[device] = toArray(rule.exec[device])
      //console.log('I AM:', my.devices)
      console.log(my[device][rule.exec[device][0]](swig.render(rule.exec[device][1]||"", {locals: my })))
    }
    if (rule.emit) {
      rule.emit = toArray(rule.emit)
      fsm.emit(rule.emit[0], swig.render(rule.emit[1]||"", {locals: my}))
    }
    if (rule.goto) fsm.transition(rule.goto)
  }
  if (rule.wait) { 
    let temp = action
    action = function(my, fsm) { after((rule.wait).seconds(), () => action(my, fsm) ) }
  }//if (rule.wait) action = (my, fsm) => after((rule.wait).seconds(), () => action(my, fsm)) 
  if (rule.loop) {
    let temp = action
    action = function(my, fsm) { 
      let e = every((rule.wait).seconds(), temp) 
      after(rule.wait * rule.loop).seconds(), () => clearInterval(e)
    }
  }
  return action 
}

var config = { 
  connections: {
  //  chip: { adaptor: "chip" },
    loopback: { adaptor: 'loopback' }
  },
  devices: {
    ping: { driver: "ping" },
    //lights: { driver: "led", pin: "XIO-PO" },
    //button: { driver: "button", pin: "XIO-P1" },
    //motion: { driver: 'analog-sensor', pin: 0, lowerLimit: 100, upperLimit: 200 }
  }
}


for (let p in iotar.pubsub) config.connections[p] = { adapter: 'mqtt', host: 'mqtt://'+iota.pubsub[p].host }


for (let s in iotar.states) {
  for (let e in iotarr.states[s]) iotar.states[s][e] = handler(iotar.states[s][e])
  console.log('AFTER:', s, iotar.states[s]._onEnter)
  //if (!iotar.states[s]._onExit) iotar.states[s]._onExit = (my) => this.rules.each((rule) => clearTimeout(rule) && clearInterval(rule))
}
//Cylon.api('http')

//iotar.namespace =  "iotar"
//iotar.initialState= iotar.inital

var fsm = new machina.BehavioralFsm({initialState: iotar.inital, states: iotar.states})
fsm.on("*", (event, data) => console.log("EVENT:", event))
//fsm.on("transition", (data) => console.log("STATE: " + data.fromState + " to " + data.toState))
  
config.work = (my) => {
  for (let p in iotar.pubsub) {
    for (let t in iotar.pubsub[p].topics) my[p].subscribe(t)
    my[p].on('message', (topic, data) => machina.emit(p+'/'+topic, data))
  }
  //my.motion.on('upperLimit', (val) => machina.emit('noMotion', val)) 
  //my.motion.on('upperLimit', (val) => machina.emit('movement', val))
  //my.button.on('release', (val) => machina.emit('buttonUp', val))
  //my.button.on('push', () => every((1).second(), () => my.led.toggle()))
  every((3).seconds(), () => fsm.handle(my, 'buttonUp'))
  //every((1).seconds(), () => my.ping.ping()) 
  after((10).seconds(), () => fsm.handle(my, 'alarm/start'))
}

// Initialize the robot 
Cylon.robot(config).start()
