# README 

IOTAR is a platform for automating the behavior of appliance and the internet of the things, using a simple portable rule language to describe behaviors.

### Getting Started


### Rules Lanaguge

in | The state the rule applies to
on | The event name that triggers this rule
at | A cron expression that must be true to trigger the rule
if | A guuard expression (swig/django/pongo syntax)
do | command to execute (see below)
emit | Event to send once the actions is done
send | Publish an event or send a remote command
rank | Priority to order rule
wait | Seconds to wait before executing
post | HTTP post

### Examples


### Components

1. A portable DSL for describing automation behaviors. 
2. An implementation using NodeJS 

### Requirements 

The following boards are supported..
* CHIP
* Beagle Bone Green (with wifi)
* Rasberry Pi 3 (coming soon)

Most Grove compatible IO devices will be supported.

### Features

* IOTA Machine config
* Web based IOTA editor
* MQTT based pub/sub
* Automatic discovery of device profiles.
* Dynamic compilation Ethereum contracts.

### Getting Started

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions
