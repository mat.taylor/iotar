brew update
brew upgrade
brew tap ethereum/ethereum
brew install ethereum
#brew tap ethereum/ethereum
#brew install cpp-ethereum
#brew linkapps cpp-ethereum


geth --identity "iota" --genesis ./eth/genesis.json --rpc --rpcport "8000" --rpccorsdomain "*" --datadir "./eth/chain" --port "30303" --nodiscover --ipcapi "admin,db,eth,debug,miner,net,shh,txpool,personal,web3" --rpcapi "db,eth,net,web3" --autodag --networkid 1900 --nat "any" console
