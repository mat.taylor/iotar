require('require-yaml')
//var Cylon = require("cylon")
var machina = require('machina')
var swig = require('swig')
var bone = require('bonescript')

var schema = {
  name: 'iotar',
  properties: {
    rules: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          in:   { type: 'string', description: 'The name of the state where this rule applies' }, 
          if:   { type: 'string', description: 'A Condition template that must evaluate true when the rule is triggered'}, 
          on:   { type: 'string', description: 'The name of an event that will trigger this rule' },
          at:   { type: 'string', description: 'A CRON expression that may be used to trigger the rule' }, 
          do:   { type: 'object', description: 'Device Commands that will be issued when the rule is triggered' },
          go:   { type: 'string', description: 'Next State to transition to after the rule has triggered' },
          emit: { type: 'string', description: 'Event to emit when the rule is triggered' },
          send: { type: 'object', description: 'Remote MQTT or HTTP event to publish' },
          wait: { type: 'integer', description: 'Seconds to wait before triggering this rule' },
          loop: { type: 'integer', description: 'Seconds to wait between repeated triggers of this rule'},
          rank: { type: 'integer', description: 'Relative prority of this rule in terms of execution order' }
        }
      }
    }
  }           
}

var iotar = {
  links: [ { name: 'home', type: 'mqtt', host: 'localhost', topics: ['general'] } ],
  start: 'sleep',
  rules: [
    { in: 'sleep', on: 'button', go: 'awake'}, 
    { in: 'awake', on: 'button', go: 'sleep', wait: 2}, 
    { in: 'sleep', do: { ping: 'ping' }},
    { in: 'awake', do: { ping: 'ping' }},
    { in: 'awake', on: 'noMove', go: 'sleep', wait: 5},
    { in: '*', on: 'motion', go: 'awake' }
  ]
}

var arif = (item) => item ? (Array.isArray(item) ? item : [item] ) : null
var rend = (temp, data) => temp ? swig.render(temp, {locals: data }) : temp

var prep = (rule) => { 
  if (!rule.in) rule.in = '*'
  if (!rule.on) rule.on = '_onEnter'
  if (!rule.do) rule.do = {}
  if (rule.do) for (var d in rule.do) rule.do[d] = arif(rule.do[d])
  if (rule.emit) rule.emit = arif(rule.emit)
  return rule
}

var load = (rule, iota) => {
  if (!iota.states[rule.in]) iota.states[rule.in] = {}
  var prior = iota.states[rule.in][rule.on]
  
  var action = function(my) {
    if (prior) prior(my)
    console.log('rule:', rule)
    if (rend(rule.if, my)) return 
    if (rule.emit) my.emit(rule.emit[0], rend(rule.emit[1], my))
    if (rule.do) for (var d in rule.do) console.log(my[d][rule.do[d][0]](rend(rule.do[d][1], my)))
    if (rule.go) my.fsm.transition(my, rule.go)
  }

  if (!rule.wait) iota.states[rule.in][rule.on] = action
  else iota.states[rule.in][rule.on] = (my) => my.timers.push(after((rule.wait).seconds(), () => action(my))) 

}

var fsm = new machina.BehavioralFsm({
  initialState: iotar.start, 
  states: { 
    '*': { 
      _onExit: (my) => { my.timers.forEach(clearTimeout); my.timers = [] } 
    } 
  }
})

fsm.on("*", (event, data) => delete data.client && console.log("EVENT:", event, data))

iotar.rules.forEach(rule => load(prep(rule), fsm))

var bot = { 
  
  connections: { beaglebone: { adaptor: 'beaglebone' }, loopback: { adaptor: 'loopback' } },

  devices: { ping: { driver: "ping" }, motion: { driver: 'direct-pin', pin: 'P9_22' } },

  work: my => { 
    my.fsm = fsm
    my.timers = []
    for (let d in bot.devices) {
      for (let e in bot.devices[d].events) my[d].on(e, (name, data) => fsm.handle(d+'/'+name, data))
    }

    for (let l of iotar.links) {
      for (let t of l.topics) my[l.name].subscribe(t)
      my[l.name].on('message', (topic, data) => my.fsm.handle(l.name+'/'+topic, data))
    }

    every((2).seconds(), () => {
      fsm.handle(my, 'button')
      console.log('MOTION:', my.motion.digitalRead()) 
    })
  }
}

for (let l of iotar.links) bot.connections[l.name] = { adapter: l.type, host: l.type+'://'+l.host }
  
Cylon.robot(bot).start()
