  name: 'iotar',
  properties: {
    rules: {
      type: 'array',
      items: {
        type: 'object',
        properties: {
          in:   { type: 'string', description: 'The name of the state where this rule applies' }, 
          if:   { type: 'string', description: 'A Condition template that must evaluate true when the rule is triggered'}, 
          on:   { type: 'string', description: 'The name of an event that will trigger this rule' },
          at:   { type: 'string', description: 'A CRON expression that may be used to trigger the rule' }, 
          do:   { type: 'object', description: 'Device Commands that will be issued when the rule is triggered' },
          go:   { type: 'string', description: 'Next State to transition to after the rule has triggered' },
          emit: { type: 'string', description: 'Event to emit when the rule is triggered' },
          send: { type: 'object', description: 'Remote MQTT or HTTP event to publish' },
          wait: { type: 'integer', description: 'Seconds to wait before triggering this rule' },
          loop: { type: 'integer', description: 'Seconds to wait between repeated triggers of this rule'},
          rank: { type: 'integer', description: 'Relative prority of this rule in terms of execution order' }
        }
      }
    }
  }           
}